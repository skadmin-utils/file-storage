<?php

declare(strict_types=1);

namespace Skadmin\FileStorage;

use Nette\Http\FileUpload;
use Nette\Utils\Strings;
use SkadminUtils\Utils\Utils\SystemDir;

use function copy;
use function explode;
use function file_exists;
use function file_put_contents;
use function filesize;
use function flush;
use function getimagesize;
use function header;
use function implode;
use function is_array;
use function is_bool;
use function is_dir;
use function md5;
use function md5_file;
use function mime_content_type;
use function pathinfo;
use function readfile;
use function sprintf;
use function str_replace;
use function trim;
use function unlink;

use const DIRECTORY_SEPARATOR;

class FileStorage
{
    /** @var mixed[] */
    private static array $TMP_SETTING = [
        'dir' => ['storage', 'files'],
    ];

    private SystemDir $systemDir;

    /** @var FilePreview[] */
    private array $filesPreview = [];

    public function __construct(SystemDir $systemDir)
    {
        $this->systemDir = $systemDir;
        $this->checkHtaccessForBaseDir();
    }

    private function checkHtaccessForBaseDir(): void
    {
        $baseDir = $this->getBaseDir();

        if (! is_dir($baseDir)) {
            $this->systemDir->createDir($baseDir, 0750);
        }

        $pathHtaccess = sprintf('%s.htaccess', $baseDir);
        if (file_exists($pathHtaccess)) {
            return;
        }

        $sourceHtaccess = implode(DIRECTORY_SEPARATOR, [__DIR__, '..', 'config', '.htaccess']);
        copy($sourceHtaccess, $pathHtaccess);
    }

    private function getBaseDir(): string
    {
        return $this->systemDir->getPathWww(self::$TMP_SETTING['dir']) . DIRECTORY_SEPARATOR;
    }

    public function getContent(string $identifier): string
    {
        return readfile($this->getFileRealPath($identifier));
    }

    public function download(string $identifier, ?string $name = null, ?string $mimeType = null, ?int $size = null): void
    {
        $filepath = $this->getFileRealPath($identifier);

        if ($name === null || trim($name) === '') {
            $pathInfo = pathinfo($filepath);
            $name     = $pathInfo['basename'];
        } elseif (! Strings::contains($name, '.')) {
            $pathInfo = pathinfo($filepath);
            $name     = sprintf('%s.%s', $name, $pathInfo['extension']); //@phpstan-ignore-line
        }

        if ($mimeType === null || trim($mimeType) === '') {
            $mimeType = mime_content_type($filepath);
        }

        if ($size === null || $size === 0) {
            $size = filesize($filepath);
        }

        header('Content-Description: File Transfer');
        header(sprintf('Content-Type: %s', $mimeType));
        header(sprintf('Content-Disposition: attachment; filename="%s"', $name));
        header('Expires: 0');
        header('Cache-Control: must-revalidate');
        header('Pragma: public');
        header(sprintf('Content-Length: %d', $size));
        flush();
        readfile($filepath);
        exit;
    }

    public function getFilePreview(string $identifier, ?string $name = null, ?string $size = null): FilePreview
    {
        $identifierWebalize = Strings::webalize($identifier);
        if (! isset($this->filesPreview[$identifierWebalize])) {
            $filepath = $this->getFileRealPath($identifier);

            if ($name === null || trim($name) === '') {
                $pathInfo = pathinfo($filepath);
                $name     = $pathInfo['basename'];
            }

            if ($size === null || trim($size) === '') {
                $_size = filesize($filepath);
                $size  = self::getSizeInUnit(is_bool($_size) ? 0 : $_size);
            }

            $isImage = is_array(getimagesize($filepath));

            $this->filesPreview[$identifierWebalize] = new FilePreview($name, $filepath, $size, $isImage);
        }

        return $this->filesPreview[$identifierWebalize];
    }

    public function remove(?string $identifier): void
    {
        if ($identifier === null) {
            return;
        }

        $path = $this->getFileRealPath($identifier);
        if (! file_exists($path)) {
            return;
        }

        unlink($path);
    }

    public function save(FileUpload $file, string $namespace): string
    {
        $sanitizedName = $file->getSanitizedName();

        $fileName = Strings::before($sanitizedName, '.', -1);
        $fileExt  = Strings::after($sanitizedName, '.', -1);

        if ($fileName === null) {
            $fileName = $sanitizedName;
        }

        $path = $this->createDir($file, $namespace);

        $filePath = $this->getFilePath($path, $fileName, $fileExt);

        $cnt = 1;
        while (file_exists($filePath)) {
            $filePath = $this->getFilePath($path, $fileName, $fileExt, $cnt++);
        }

        $file->move($filePath);

        return explode($this->getBaseDir(), $filePath)[1]; //@phpstan-ignore-line
    }

    public function saveContent(string $content, string $name, string $namespace, bool $force = false): string
    {
        $fncSanitizedName = static function (string $name): string {
            $name = Strings::webalize($name, '.', false);
            $name = str_replace(['-.', '.-'], '.', $name);
            $name = trim($name, '.-');
            $name = $name === '' ? 'unknown' : $name;

            return $name;
        };
        $sanitizedName    = $fncSanitizedName($name);

        $fileName = Strings::before($sanitizedName, '.', -1);
        $fileExt  = Strings::after($sanitizedName, '.', -1);

        if ($fileName === null) {
            $fileName = $sanitizedName;
        }

        $path = $this->createDir($name . $namespace, $namespace);

        $filePath = $this->getFilePath($path, $fileName, $fileExt);

        if ($force) {
            $this->remove(explode($this->getBaseDir(), $filePath)[1]);
        }

        $cnt = 1;
        while (file_exists($filePath)) {
            $filePath = $this->getFilePath($path, $fileName, $fileExt, $cnt++);
        }

        file_put_contents($filePath, $content);

        return explode($this->getBaseDir(), $filePath)[1]; //@phpstan-ignore-line
    }

    private function createDir(FileUpload|string $fileOrString, string $namespace): string
    {
        $dir = self::$TMP_SETTING['dir'];

        $md5File = $fileOrString instanceof FileUpload ? md5_file($fileOrString->getTemporaryFile()) : md5($fileOrString);

        $dir[] = Strings::webalize($namespace);
        $dir[] = Strings::substring($md5File, 0, 2); //@phpstan-ignore-line

        $path = $this->systemDir->getPathWww($dir);

        $this->systemDir->createDir($path, 0755);

        return $path;
    }

    private function getFilePath(string $path, string $fileName, ?string $fileExt, int $index = 0): string
    {
        if ($index !== 0) {
            $fileName .= sprintf('.%d', $index);
        }

        if ($fileExt !== null) {
            $fileName .= sprintf('.%s', $fileExt);
        }

        return sprintf('%s%s%s', $path, DIRECTORY_SEPARATOR, $fileName);
    }

    private function getFileRealPath(string $identifier): string
    {
        return sprintf('%s%s', $this->getBaseDir(), $identifier);
    }

    public static function getSizeInUnit(int $size): string
    {
        foreach (['kB', 'MB', 'GB', 'TB'] as $unit) {
            $size /= 1024;

            if ($size < 1024) {
                return sprintf('%.02f %s', $size, $unit);
            }
        }

        return sprintf('%.02f %s', $size, $unit ?? 'B');
    }
}
