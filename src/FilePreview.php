<?php

declare(strict_types=1);

namespace Skadmin\FileStorage;

use function base64_encode;
use function file_get_contents;
use function is_bool;
use function pathinfo;

use const PATHINFO_EXTENSION;

class FilePreview
{
    private string $name;

    private string $path;

    private string $size;

    private bool $isImage;

    public function __construct(string $name, string $path, string $size, bool $isImage)
    {
        $this->name    = $name;
        $this->path    = $path;
        $this->size    = $size;
        $this->isImage = $isImage;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function getBase64(): string
    {
        if (! $this->isImage()) {
            return '';
        }

        $type = pathinfo($this->path, PATHINFO_EXTENSION);
        $data = file_get_contents($this->path);

        if (is_bool($data)) {
            return '';
        }

        return 'data:image/' . $type . ';base64,' . base64_encode($data);
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function isImage(): bool
    {
        return $this->isImage;
    }
}
